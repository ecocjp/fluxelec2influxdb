package config

import (
	"io/ioutil"
	
	"gopkg.in/yaml.v2"
)
// Statistics collected

type FluxElec struct {
	Services []Service `json:"services"`
}

type Service struct {
	NbAppels			int		`json:"nbAppels"`
	NbExceptions		int		`json:"nbException"`
	NbSuccess			int		`json:"nbSucces"`
	TempsExecutionMaxi	int		`json:"tempsExecutionMax"`
	TempsExecutionMini	int		`json:"tempsExecutionMin"`
	TotalPieces			int		`json:"totalPieces"`
	TotalTaillePieces	int		`json:"totalTaillePieces"`
	TotalTemps			int		`json:"totalTemps"`
	Service				string	`json:"service"`
}

// InfluxDB server connect

type Storage struct {
	Server		string	`yaml:"server,omitempty"`
	Port		string	`yaml:"port,omitempty"`
	User		string	`yaml:"user,omitempty"`
	Password	string	`yaml:"pwd,omitempty"`
	Base		string	`yaml:"base,omitempty"`
	Retention	string	`yaml:"retention,omitempty"`
}

type Appli struct {
	URLS		[]string	`yaml:"urls,omitempty"`
	Freq		int			`yaml:"freq,omitempty"`
}

type Config struct {
	InfluxCnx	Storage		`yaml:"influxdb,omitempty"`
	Applis		[]Appli		`yaml:"applications,omitempty"`
}
// Parse a file into a config instance
func Parse(f string) (config Config, err error) {
	bts, err := ioutil.ReadFile(f)
	if err != nil {
		return
	}
	err = yaml.Unmarshal(bts, &config)

	return
}

