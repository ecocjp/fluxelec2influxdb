package influx

import (
	"time"
	
	"github.com/apex/log"

	_ "github.com/influxdata/influxdb1-client"
	client "github.com/influxdata/influxdb1-client/v2"

	"gitlab.com/ecocjp/fluxelec2influxdb/config"
)

const (
	TEMPO		 = 600		// Nombre de secondes maximum entres deux envois.
	MAX_MEASURE	 = 1		// Nombre de mesures maximum stockées avant envois (~400sec).
)

type BaseInfos struct {
	// client	InfluxDB connection handler
	client	client.Client
	// batchPointsConf configuration data needed to create 
	// a BatchPoints instance. At least Database name and Precision.
	batchPointsConf	client.BatchPointsConfig
}

type LastValue struct {
	// nextSendMap time of of next write
	nextSendMap			time.Time
	// lastIsOpen	value of last isOpen data
	lastIsOpen			bool
}

type StorageInfos struct {
	// batchPointsTable give access to BatchPoints struct for each cluster
	batchPointsTable	client.BatchPoints
	// batchPointsNumber store BatchPoints number for each cluster
	batchPointsNumber	int
	// lastValues store time of next write and last value of isOpen
	lastValues			map[string]*LastValue
}

// getNewBatch creates a new batch points table for influxDB.
func (tf *BaseInfos)getNewBatch() ( client.BatchPoints, error ) {
	return client.NewBatchPoints( tf.batchPointsConf )
}

// QueryDB will send query to InfluxDB database.
func (tf *BaseInfos)QueryDB( q client.Query ) (*client.Response, error) {
	var ctxLog = log.WithField("base", tf.batchPointsConf.Database)

	response, err := tf.client.Query( q )

	if err != nil {
		ctxLog.WithError(err).Error( "QueryDB error" )
	} else if response.Error() != nil {
		ctxLog.WithError(response.Error()).Error( "QueryDB response error" )
	} else {
		ctxLog.Debugf( "QueryDB response: %v.\n", response.Results )
	}

	if err != nil {
		return nil, err
	} else if response.Error() != nil {
		return nil, response.Error()
	} else {
		return response, nil
	}
}

// CreateDB is used to create a new database.
// If database already exists, no error will return.
func (tf *BaseInfos)CreateDB() error {
	var ctxLog = log.WithField("base", tf.batchPointsConf.Database)

	q := client.NewQuery( "CREATE DATABASE " + tf.batchPointsConf.Database, "", "" )
	response, err := tf.client.Query( q )
	if err != nil {
		return err
	}
	ctxLog.Debugf( "CREATE DATABASE %s: %s.\n", tf.batchPointsConf.Database, response.Results )

	return nil
}

// CreateRetentionPolicy creates an InfluDB database retention policy 
// These becomes new Stockage default retention.
func (tf *BaseInfos)CreateRetentionPolicy( ret_name string, duration string, defaut bool ) error {
	var ctxLog = log.WithField("base", tf.batchPointsConf.Database)

	ctxLog.Debugf( "SetRetentionPolicy(%s,%s,%v)\n", ret_name, duration, defaut )

	cmd := "DURATION " + duration + "REPLICATION 1"
	if defaut {
		cmd += " DEFAULT"
	}

	query := "CREATE RETENTION POLICY \"" + ret_name + "\" ON \"" + tf.batchPointsConf.Database + "\" " + cmd
	q := client.NewQuery( query, "", "" )
	response, err := tf.client.Query( q )
	if err != nil || response.Error() != nil {
		if err != nil {
			return err
		} else {
			return response.Error()
		}
	}

	tf.batchPointsConf.RetentionPolicy = ret_name

	return nil
}

// Flush send batch points to InfluxDB.
func (tf *BaseInfos)Flush( st *StorageInfos ) error {
	err := tf.client.Write( st.batchPointsTable )
	if err != nil {
		return err
	}
	bp, err := tf.getNewBatch()
	if err != nil {
		return err
	}
	st.batchPointsNumber = 0;
	st.batchPointsTable = bp

	return nil
}
// NewStorageInfos creates a new batchpoints for each cluster because
// threading requirements.
//
func NewStorageInfos( base *BaseInfos ) (*StorageInfos, error) {
	var ctxLog = log.WithField("base", base.batchPointsConf.Database)
	var err error

	tf := new(StorageInfos)
	tf.batchPointsTable, err = client.NewBatchPoints( base.batchPointsConf )
	if err != nil {
		ctxLog.WithError(err).Error( "NewBatchPoints" )
		return nil,err
	}
	tf.batchPointsNumber = 0

	return tf,nil
}

// ConnectToInflux creates connexion context with InfluxDB server.
func ConnectToInflux( conf config.Config ) *BaseInfos {
	var ctxLog = log.WithField("server", conf.InfluxCnx.Server).WithField("base", conf.InfluxCnx.Base)
	var err error

	stock := new(BaseInfos)
	addresse := "http://" + conf.InfluxCnx.Server + ":" + conf.InfluxCnx.Port
	if conf.InfluxCnx.User != "" {
		stock.client, err = client.NewHTTPClient(client.HTTPConfig{
					Addr: addresse,
					Username: conf.InfluxCnx.User,
					Password: conf.InfluxCnx.Password,
		})
	} else {
		stock.client, err = client.NewHTTPClient(client.HTTPConfig{
					Addr: addresse,
		})
	}
	if err != nil {
		ctxLog.WithError(err).Fatal("failed to connect InfluxDB")
	}

	defer stock.client.Close()

	stock.batchPointsConf = client.BatchPointsConfig{
					Database: conf.InfluxCnx.Base,
					Precision: "ns",
	}

	err = stock.CreateDB()
	if err != nil {
		ctxLog.WithError(err).Fatal("failed to create database")
	}

	if conf.InfluxCnx.Retention != "" {
		stock.batchPointsConf.RetentionPolicy = conf.InfluxCnx.Retention
	}

	return stock
}

// AddMeasures add measurement into batchPointsTable and send datas on regular interval.
//
// Format de line: measurement{tag_name=tag_value[,tag_name=tag_value,...]} field_value
//
func AddMeasures( tf *BaseInfos, st *StorageInfos, stats *config.FluxElec, host string, port string ) error {
	var ctxLog = log.WithField("host", host).WithField("base", tf.batchPointsConf.Database)

	stamp := time.Now()

	ctxLog.Infof( "AddMeasures: %v now: %v", stats, stamp )
	
	for _, stat := range stats.Services {
		tags := map[string]string {
			"host": host,
			"port": port,
			"service": stat.Service,
		}
	
		fields := map[string]interface{}{
			"nbAppels":stat.NbAppels,
			"nbExceptions":stat.NbExceptions,
			"nbSuccess":stat.NbAppels,
			"tempsExecutionMaxi":stat.TempsExecutionMaxi,
			"tempsExecutionMini":stat.TempsExecutionMini,
			"totalPieces":stat.TotalPieces,
			"totalTaillePieces":stat.TotalTaillePieces,
			"totalTemps":stat.TotalTemps,
		}

		pt, err := client.NewPoint( "stats", tags, fields, stamp )
		if err != nil {
			ctxLog.WithError(err).Warn("failed to create point")
		} else {
			st.batchPointsTable.AddPoint(pt)
			st.batchPointsNumber++;
		}
	}
	// Send infos to InfluxDB
	ctxLog.Debugf( "Sending %d measurements to InfluxDB at %s", st.batchPointsNumber, stamp.String() )

	err := tf.Flush( st )
	if err != nil {
		ctxLog.WithError(err).Error( "Flushing measurements" )
	}

	return err
}
