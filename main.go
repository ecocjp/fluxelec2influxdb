package main 

import (
	"os"
	"fmt"
	"flag"
	"time"
	"io/ioutil"
	"net/http"
	"net/url"
	"html/template"
	"encoding/json"
	
	"github.com/apex/log"
	"github.com/apex/log/handlers/cli"

	"gitlab.com/ecocjp/fluxelec2influxdb/config"
	"gitlab.com/ecocjp/fluxelec2influxdb/influx"
)

var Version string
var Build string

var debug = flag.Bool( "debug", false, "Show debug log" )
var configFile = flag.String( "config", "config.yml", "config file" )
var listenPort = flag.String( "port", ":9445", "Local http server port (care of ':'!)" )
var version = flag.Bool( "version", false, "Prints current version." )

var indexTmpl = `
<html>
<head><title>Fluxelec to InfluxDB agent</title></head>
<body>
	<h1>Fluxelec to InfluxDB</h1>
	<h3>Informations</h3>
	<ul>
	<li>Version {{.Version}}</li>
	<li>Build : {{.Build}}</li>
	</ul>
	<h3>Configuration</h3>
	<a href="/config">Voir la configuration courante.</a>
</body>
</html>
`
var configTmpl = `
<html>
<head><title>Fluxelec to InfluxDB agent</title></head>
<body>
	<h1>Fluxelec to InfluxDB: Configuration</h1>
	<h3>Configuration</h3>
	<ul>
	 {{if .InfluxCnx.Server}} <li>Serveur: {{.InfluxCnx.Server}}:{{.InfluxCnx.Port}} </li> {{end}}
	 {{if .InfluxCnx.User}} <li>User     : {{.InfluxCnx.User}} </li> {{end}}
	 {{if .InfluxCnx.Base}} <li>Base     : {{.InfluxCnx.Base}} </li> {{end}}
	 {{if .InfluxCnx.Retention}} <li>Retention: {{.InfluxCnx.Retention}} </li> {{end}}
	</ul>
	<h3>URLs being monitored:</h3>
	<ul>
	{{ range .Applis }}
		{{ range .URLS }}
			<li><a href="{{.}}">{{ .}}</a></li>
		{{ end }}
	{{ end }}
	<ul>
</body>
</html>
`

func Collecte( myurl string, freq int, unflux *influx.BaseInfos ) {
	ctxLog := log.WithFields( log.Fields{
			"appli": "fluxelec2influxdb",
	})
	ctxLog.Infof( "Start collect for %s each %d seconds.", myurl, freq )

	storage, err := influx.NewStorageInfos( unflux )
	if err != nil {
		ctxLog.WithError(err).Fatal("NewStorageInfos")
	}
	u, err := url.Parse(myurl)
	if err != nil {
		ctxLog.WithError(err).Fatal( "Collect" )
		return
	}
	host := u.Hostname()
	port := u.Port()
	
	for {
		time.Sleep( time.Duration(freq) * time.Second )
		// Get URL
		ctxLog.Infof( "GET %s", myurl )
		res, err := http.Get( myurl )
		if err != nil {
			ctxLog.WithError(err).Fatal("Collecte")
			continue
		}
		json_resp, err := ioutil.ReadAll(res.Body)
		res.Body.Close()
		if err != nil {
			ctxLog.WithError(err).Fatal("Collecte")
			continue
		}
		ctxLog.Infof( "RECVD %s characters.", len(json_resp) )
		// Parse JSON
		var stats config.FluxElec
		err = json.Unmarshal( json_resp, &stats )
		if err != nil {
			ctxLog.WithError(err).Fatal("JSON Parse")
			continue
		}
		influx.AddMeasures( unflux, storage, &stats, host, port )
	}
}

func init() {
	log.SetHandler(cli.Default)
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf( os.Stderr, "usage: fluxelec2influxdb [option]\n" );
		flag.PrintDefaults()
		os.Exit(2)
	}
	flag.Parse()

	if *version {
		fmt.Fprintf( os.Stdout, "Version: %s\n", Version )
		fmt.Fprintf( os.Stdout, "Build Time: %s\n", Build )
		os.Exit( 0 )
	}

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.ErrorLevel)
	}
	
	ctxLog := log.WithFields( log.Fields{
			"appli": "fluxelec2influxdb",
	})
	
	// Parse config file
	conf, err := config.Parse(*configFile)
	if err != nil {
		ctxLog.WithError(err).Fatalf("failed to parse config file: %s", *configFile)
	}
	ctxLog.Infof( "Configuration:\n%#v\n", conf )
	
	// Open connection to InfluxDB
	unflux := influx.ConnectToInflux( conf )
	ctxLog.Infof( "Connected to InfluxDB:\n%#v\n", unflux )

	// Sart collectors
	for _, appli := range conf.Applis {
		for _, url := range appli.URLS {
			go Collecte( url, appli.Freq, unflux )
		}
	}
	// Start web server
	var mux = http.NewServeMux()
	var index = template.Must(template.New("index").Parse(indexTmpl))
	var config = template.Must(template.New("config").Parse(configTmpl))
	mux.HandleFunc( "/",
					func( w http.ResponseWriter, r *http.Request) {
						if err := index.Execute(w, map [string]interface{}{
								"Version": Version, "Build": Build} ); err != nil {
							http.Error(w, err.Error(), http.StatusInternalServerError)
						}
					})
	mux.HandleFunc( "/config",
					func( w http.ResponseWriter, r *http.Request) {
						if err := config.Execute(w, &conf); err != nil {
							http.Error(w, err.Error(), http.StatusInternalServerError)
						}
					})
	ctxLog.Info( "Starting http server" );
	if err := http.ListenAndServe(*listenPort, mux); err != nil {
		ctxLog.WithError(err).Fatal("failed to start server")
	}
}

