# FluxElec Statistics exporter to InfluxDB.

fluxelec2influxdb exports servlet statistics to InfluxDB infrastructure.

Installation:

    Get source from this gitlab repository
    make
    Copy fluxelect2influxdb executable
    Modify config.yml as needed (URLs, InfluxDB connection)

Option:

    fluxelec2influxdb --config autre_config.yml


Configuration:

    Create an YAML file like this:
    
```yaml
influxdb:
    server: @IP du serveur InfluxDB
    port: Port du serveur (usually 8086)
    user: database username or empty
    pwd: user password or empty
    base: database name

application:
- freq: 60
  urls: ['http://machine:port/path',
        'http://next URL']
```
- In 'application' stanza, be carefull with blanks and minus, it's YAML syntax ! :-)
- If omitted freq = 10 seconds.
