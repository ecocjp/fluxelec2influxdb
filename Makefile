BINARY=fluxelec2influxdb

VERSION=`git describe --tags`
##VERSION='1.0.0'
BUILD=`date +%FT%T%z`

LDFLAGS=-ldflags "-w -s -X main.Version=${VERSION} -X main.Build=${BUILD}"

build:
	go build ${LDFLAGS} -o ${BINARY}

install:
	go install ${LDFLAGS}

clean:
	$(RM) ${BINARY}

.PHONY: clean install
